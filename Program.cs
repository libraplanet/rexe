using System;
using System.Diagnostics;
using System.Collections.Generic;

class Program
{
	class NotProcessArgumentException : Exception {}

	class PathMix
	{
		string path;

		public PathMix(string path)
		{
			this.path = path;
		}

		public string OriginalPath
		{
			get
			{
				return path;
			}
		}

		public string FullPath
		{
			get
			{
				return new Uri(path).LocalPath;
			}
		}
	}

	static void Main(string[] args)
	{
		Console.WriteLine("------------------------------------------");
		Console.WriteLine("  application : rexe");
		Console.WriteLine("  auther      : takumi");
		Console.WriteLine("  date        : 12/02/20");
		Console.WriteLine("------------------------------------------");

		try
		{
			PathMix exeFile, workDir;
			List<Process> list = new List<Process>();
			//args
			{
				Console.WriteLine("[args]");
				if(args.Length >= 1)
				{
					//exe
					{
						exeFile = new PathMix(args[0]);
						Console.WriteLine("exe file = " + exeFile.FullPath + "(" + exeFile.OriginalPath + ")");
					}

					//work dir
					if(args.Length >= 2)
					{
						workDir = new PathMix(args[1]);
						Console.WriteLine("work dir = " + workDir.FullPath + "(" + workDir.OriginalPath + ")");
					}
					else
					{
						workDir = null;
						Console.WriteLine("work dir = [N/A]");
					}
				}
				else
				{
					throw new NotProcessArgumentException();
				}
				Console.WriteLine(" ");
			}
			{
				Console.WriteLine("[process]");
				//serch
				{
					foreach(Process p in Process.GetProcesses())
					{
						try
						{
							if(new PathMix(p.MainModule.FileName).FullPath == exeFile.FullPath)
							{
								Console.WriteLine("fined id("+ p.Id +")");
								list.Add(p);
							}
						}
						catch
						{
						}
					}
				}

				//kill
				{
					foreach(Process p in list)
					{
						int id = p.Id;
						p.Kill();
						Console.WriteLine("killed id("+ id +")");
					}
				}

				//start
				{
					Process p = new Process();
					p.StartInfo.FileName = exeFile.FullPath;
					if(workDir != null)
					{
						p.StartInfo.WorkingDirectory = workDir.FullPath;
					}
					p.Start();
					Console.WriteLine("started id("+ p.Id +")");
				}
			}
		}
		catch(NotProcessArgumentException)
		{
			Console.WriteLine("No Argument...");
			Console.WriteLine("");
			Console.WriteLine("[help]");
			Console.WriteLine("usage: rexe [exe file]");
			Console.WriteLine("usage: rexe [exe file] [work dir]");
			Console.WriteLine("");
			Console.WriteLine("[exe file] : target execute file path.");
			Console.WriteLine("[work dir] : working dir path. (not must)");
		}
		catch(Exception e)
		{
			Console.WriteLine("");
			Console.WriteLine(e.ToString());
		}
		finally
		{
		}
	}
}